package com.mobithink.velo.carbon.database.model;

import java.io.Serializable;

public class EventDTO implements Serializable {

    private Long eventId;
    private String eventName;
    private Long startTime;
    private Long endTime;
    private Double gpsLatStart;
    private Double gpsLongStart;
    private Double gpsLatEnd;
    private Double gpsLongEnd;
    private String picture;
    private String voiceMemo;
    private String eventType;
    private Long tripId;

    public EventDTO() {
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Double getGpsLatStart() {
        return gpsLatStart;
    }

    public void setGpsLatStart(Double gpsLatStart) {
        this.gpsLatStart = gpsLatStart;
    }

    public Double getGpsLongStart() {
        return gpsLongStart;
    }

    public void setGpsLongStart(Double gpsLongStart) {
        this.gpsLongStart = gpsLongStart;
    }

    public Double getGpsLatEnd() {
        return gpsLatEnd;
    }

    public void setGpsLatEnd(Double gpsLatEnd) {
        this.gpsLatEnd = gpsLatEnd;
    }

    public Double getGpsLongEnd() {
        return gpsLongEnd;
    }

    public void setGpsLongEnd(Double gpsLongEnd) {
        this.gpsLongEnd = gpsLongEnd;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getVoiceMemo() {
        return voiceMemo;
    }

    public void setVoiceMemo(String voiceMemo) {
        this.voiceMemo = voiceMemo;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }
}
