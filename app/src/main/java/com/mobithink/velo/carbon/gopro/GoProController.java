package com.mobithink.velo.carbon.gopro;


import android.content.Context;
import android.util.Log;

import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class GoProController implements Callback<ResponseBody> {

    private final Context context;
    private final GoProAPI goProAPI;
    private final OnTaskCompleted listener;


    public GoProController(Context c, OnTaskCompleted listener) {
        context=c;
        Retrofit client =  GoProApiClient.getClient();
        goProAPI=client.create(GoProAPI.class);
        this.listener=listener;
    }

    public void startTakePhoto(){
        Call<ResponseBody> call = goProAPI.takePhoto();
        call.enqueue(this);
    }


    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()){
            listener.onTaskCompleted(true);
        }
        else {
            listener.onTaskCompleted(false);
        }

    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Request request = call.request();
        Log.e(this.getClass().getName(),"error");
    }
}
