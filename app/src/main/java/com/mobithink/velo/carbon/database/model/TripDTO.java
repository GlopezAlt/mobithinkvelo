package com.mobithink.velo.carbon.database.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jpaput on 07/02/2017.
 */

public class TripDTO implements Serializable {

    public Long tripID;
    private String tripName;
    private Long startTime;
    private Long endTime;
    private String atmo;
    private String temperature;
    private String weather;
    private double startGpsLat;
    private double startGpsLong;
    private double endGpsLat;
    private double endGpsLong;
    private String city;

    public List<RollingPointDTO> rollingPoints;
    public List<EventDTO> events;

    public TripDTO() {
    }

    public Long getTripID() {
        return tripID;
    }

    public void setTripID(Long tripID) {
        this.tripID = tripID;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public List<RollingPointDTO> getRollingPoints() {
        return rollingPoints;
    }

    public void setRollingPoints(List<RollingPointDTO> rollingPoints) {
        this.rollingPoints = rollingPoints;
    }

    public List<EventDTO> getEvents() {
        return events;
    }

    public void setEvents(List<EventDTO> events) {
        this.events = events;
    }

    public String getAtmo() {
        return atmo;
    }

    public void setAtmo(String atmo) {
        this.atmo = atmo;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public double getStartGpsLat() {
        return startGpsLat;
    }

    public void setStartGpsLat(double startGpsLat) {
        this.startGpsLat = startGpsLat;
    }

    public double getStartGpsLong() {
        return startGpsLong;
    }

    public void setStartGpsLong(double startGpsLong) {
        this.startGpsLong = startGpsLong;
    }

    public double getEndGpsLat() {
        return endGpsLat;
    }

    public void setEndGpsLat(double endGpsLat) {
        this.endGpsLat = endGpsLat;
    }

    public double getEndGpsLong() {
        return endGpsLong;
    }

    public void setEndGpsLong(double endGpsLong) {
        this.endGpsLong = endGpsLong;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
