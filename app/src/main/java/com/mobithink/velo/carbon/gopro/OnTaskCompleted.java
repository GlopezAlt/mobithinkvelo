package com.mobithink.velo.carbon.gopro;

public interface OnTaskCompleted {

    void onTaskCompleted(boolean wasPhotoTaken);
}
