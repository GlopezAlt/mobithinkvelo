package com.mobithink.velo.carbon.managers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mobithink.velo.carbon.CarbonApplication;
import com.mobithink.velo.carbon.database.DatabaseOpenHelper;
import com.mobithink.velo.carbon.database.model.EventDTO;
import com.mobithink.velo.carbon.database.model.RollingPointDTO;
import com.mobithink.velo.carbon.database.model.TripDTO;

import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

    private static final String TAG = "DatabaseManager";

    private static DatabaseManager mInstance;

    private static DatabaseOpenHelper mDataBase;
    private SQLiteDatabase openedDatabase;

    public static DatabaseManager getInstance() {
        if (mInstance == null) {
            mInstance = new DatabaseManager();
            mDataBase = new DatabaseOpenHelper(CarbonApplication.getInstance());
        }
        return mInstance;
    }

    public void open() {
        openedDatabase = mDataBase.getWritableDatabase();
    }

    private SQLiteDatabase getOpenedDatabase() {
        if (openedDatabase == null) {
            open();
        }
        return openedDatabase;
    }

    /*************** TripDTO
     * @param tripDTO**************/

    public void startNewTrip(TripDTO tripDTO) {
        ContentValues values = new ContentValues();
        values.put(DatabaseOpenHelper.KEY_START_DATETIME, getDateTime());
        values.put(DatabaseOpenHelper.KEY_START_LATITUDE,tripDTO.getStartGpsLat());
        values.put(DatabaseOpenHelper.KEY_START_LONGITUDE,tripDTO.getStartGpsLong());

        // insert row
        long tripId = getOpenedDatabase().insert(DatabaseOpenHelper.TABLE_TRIP, null, values);

        Log.i(TAG, "A new Trip have been created : tripID = " + tripId);

        CarbonApplicationManager.getInstance().setCurrentTripId(tripId);
    }

    public long updateTrip(TripDTO tripDTO) {
        ContentValues values = new ContentValues();
        values.put(DatabaseOpenHelper.KEY_END_DATETIME, getDateTime());
        values.put(DatabaseOpenHelper.KEY_END_LATITUDE,tripDTO.getEndGpsLat());
        values.put(DatabaseOpenHelper.KEY_END_LONGITUDE,tripDTO.getEndGpsLong());

        int sqlReturn = getOpenedDatabase().update(DatabaseOpenHelper.TABLE_TRIP, values, DatabaseOpenHelper.KEY_ID + " = ?",
                new String[]{String.valueOf(tripDTO.tripID)});

        return sqlReturn;
    }

    TripDTO getTrip(long tripId) {

        String selectQuery = "SELECT  * FROM " + DatabaseOpenHelper.TABLE_TRIP + " WHERE "
                + DatabaseOpenHelper.KEY_ID + " = " + tripId;

        Cursor c = getOpenedDatabase().rawQuery(selectQuery, null);

        if (c == null) {
            return null;
        }

        c.moveToFirst();

        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripID(c.getLong(c.getColumnIndex(DatabaseOpenHelper.KEY_ID)));
        tripDTO.setTripName((c.getString(c.getColumnIndex(DatabaseOpenHelper.KEY_TRIP_NAME))));
        tripDTO.setStartTime(c.getLong(c.getColumnIndex(DatabaseOpenHelper.KEY_START_DATETIME)));
        tripDTO.setEndTime(c.getLong(c.getColumnIndex(DatabaseOpenHelper.KEY_END_DATETIME)));
        tripDTO.setStartGpsLat(c.getLong(c.getColumnIndex(DatabaseOpenHelper.KEY_START_LATITUDE)));
        tripDTO.setStartGpsLong(c.getLong(c.getColumnIndex(DatabaseOpenHelper.KEY_START_LONGITUDE)));
        tripDTO.setEndGpsLat(c.getLong(c.getColumnIndex(DatabaseOpenHelper.KEY_END_LATITUDE)));
        tripDTO.setEndGpsLong(c.getLong(c.getColumnIndex(DatabaseOpenHelper.KEY_END_LONGITUDE)));
        tripDTO.setAtmo(c.getString(c.getColumnIndex(DatabaseOpenHelper.KEY_ATMO)));
        tripDTO.setTemperature(c.getString(c.getColumnIndex(DatabaseOpenHelper.KEY_TEMPERATURE)));
        tripDTO.setCity(c.getString(c.getColumnIndex(DatabaseOpenHelper.KEY_CITY)));
        tripDTO.setWeather(c.getString(c.getColumnIndex(DatabaseOpenHelper.KEY_WEATHER)));
        c.close();

        return tripDTO;
    }

    public TripDTO getFullTripDTODataToSend(long tripId) {

        TripDTO tripDto = getTrip(tripId);

        tripDto.rollingPoints = getRollingPointsForTripId(tripId);
        tripDto.events = getEventsForTripId(tripId);

        return tripDto;
    }


    /***************
     * Rolling Point
     *****************/

    public void registerRollingPoint(RollingPointDTO rollingPointDTO) {

        ContentValues values = new ContentValues();
        values.put(DatabaseOpenHelper.KEY_TRIP_ID, rollingPointDTO.getTripId());
        values.put(DatabaseOpenHelper.KEY_START_LONGITUDE, rollingPointDTO.getGpsLong());
        values.put(DatabaseOpenHelper.KEY_START_LATITUDE, rollingPointDTO.getGpsLat());
        values.put(DatabaseOpenHelper.KEY_CREATION_DATE, getDateTime());

        // insert row
        long rollingPointId = getOpenedDatabase().insert(DatabaseOpenHelper.TABLE_ROLLING_POINT, null, values);

        Log.i(TAG, "A new rollingPoint have been created : tripID = " + rollingPointId + " ,with longitude [" + rollingPointDTO.getGpsLong() + "] and lattitude [" + rollingPointDTO.getGpsLat() + "]");
    }

    private List<RollingPointDTO> getRollingPointsForTripId(long tripId) {

        List<RollingPointDTO> rollingPoints = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + DatabaseOpenHelper.TABLE_ROLLING_POINT + " WHERE "
                + DatabaseOpenHelper.KEY_TRIP_ID + " = " + tripId;

        Cursor cursor = getOpenedDatabase().rawQuery(selectQuery, null);

        if (cursor == null) {
            return null;
        }

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                RollingPointDTO rp = new RollingPointDTO();

                rp.setTripId(tripId);
                rp.setGpsLong(cursor.getDouble(cursor.getColumnIndex(DatabaseOpenHelper.KEY_START_LONGITUDE)));
                rp.setGpsLat(cursor.getDouble(cursor.getColumnIndex(DatabaseOpenHelper.KEY_START_LATITUDE)));
                rp.setTimeOfRollingPoint(cursor.getLong(cursor.getColumnIndex(DatabaseOpenHelper.KEY_CREATION_DATE)));

                rollingPoints.add(rp);

                cursor.moveToNext();
            }
        }
        cursor.close();

        return rollingPoints;
    }


    /***************************
     * Events
     ****************************/

    private List<EventDTO> getEventsForTripId(long tripId) {

        List<EventDTO> eventDTOList = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + DatabaseOpenHelper.TABLE_EVENT + " WHERE "
                + DatabaseOpenHelper.KEY_TRIP_ID + " = " + tripId;

        Cursor cursor = getOpenedDatabase().rawQuery(selectQuery, null);

        if (cursor == null) {
            return null;
        }

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                EventDTO ev = new EventDTO();

                ev.setEventName(cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.KEY_EVENT_NAME)));
                ev.setStartTime(cursor.getLong(cursor.getColumnIndex(DatabaseOpenHelper.KEY_START_DATETIME)));
                ev.setEndTime(cursor.getLong(cursor.getColumnIndex(DatabaseOpenHelper.KEY_END_DATETIME)));
                ev.setGpsLongStart(cursor.getDouble(cursor.getColumnIndex(DatabaseOpenHelper.KEY_START_LONGITUDE)));
                ev.setGpsLatStart(cursor.getDouble(cursor.getColumnIndex(DatabaseOpenHelper.KEY_START_LATITUDE)));
                ev.setGpsLongEnd(cursor.getDouble(cursor.getColumnIndex(DatabaseOpenHelper.KEY_END_LONGITUDE)));
                ev.setGpsLatEnd(cursor.getDouble(cursor.getColumnIndex(DatabaseOpenHelper.KEY_END_LATITUDE)));
                ev.setVoiceMemo(cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.KEY_VOICE_MEMO)));
                ev.setPicture(cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.KEY_PICTURE)));
                ev.setEventType(cursor.getString(cursor.getColumnIndex(DatabaseOpenHelper.KEY_EVENT_TYPE)));

                eventDTOList.add(ev);
                cursor.moveToNext();
            }
        }

        cursor.close();

        return eventDTOList;
    }

    /*************************** Utils *****************************/
    private Long getDateTime() {
        return System.currentTimeMillis();
    }


    /***************************
     * EVENT
     **************************************/

    public long createNewEvent(EventDTO eventDTO) {

        ContentValues values = new ContentValues();
        values.put(DatabaseOpenHelper.KEY_TRIP_ID, eventDTO.getTripId());
        values.put(DatabaseOpenHelper.KEY_EVENT_NAME, eventDTO.getEventName());
        values.put(DatabaseOpenHelper.KEY_START_DATETIME, eventDTO.getStartTime());
        values.put(DatabaseOpenHelper.KEY_START_LATITUDE, eventDTO.getGpsLatStart());
        values.put(DatabaseOpenHelper.KEY_START_LONGITUDE, eventDTO.getGpsLongStart());
        values.put(DatabaseOpenHelper.KEY_END_DATETIME, eventDTO.getEndTime());
        values.put(DatabaseOpenHelper.KEY_END_LATITUDE, eventDTO.getGpsLatEnd());
        values.put(DatabaseOpenHelper.KEY_END_LONGITUDE, eventDTO.getGpsLongEnd());
        values.put(DatabaseOpenHelper.KEY_EVENT_TYPE, eventDTO.getEventType());
        values.put(DatabaseOpenHelper.KEY_VOICE_MEMO, eventDTO.getVoiceMemo());
        values.put(DatabaseOpenHelper.KEY_PICTURE, eventDTO.getPicture());

        long eventId = getOpenedDatabase().insert(DatabaseOpenHelper.TABLE_EVENT, null, values);
        Log.i(TAG, "An event has been created : tripID = " + eventId + ", for TripId " + eventDTO.getTripId() + ", with endTime " + eventDTO.getEndTime());

        return eventId;
    }

    public int updateEvent(EventDTO eventDTO) {
        ContentValues values = new ContentValues();
        values.put(DatabaseOpenHelper.KEY_END_DATETIME, eventDTO.getEndTime());
        values.put(DatabaseOpenHelper.KEY_END_LATITUDE, eventDTO.getGpsLatEnd());
        values.put(DatabaseOpenHelper.KEY_END_LONGITUDE, eventDTO.getGpsLongEnd());


        try {
            int value = getOpenedDatabase().update(DatabaseOpenHelper.TABLE_EVENT, values, DatabaseOpenHelper.KEY_ID + " = ?",
                    new String[]{String.valueOf(eventDTO.getEventId())});
            Log.i(TAG, "A event has been updated : tripID = " + eventDTO.getEventId() + ", for TripId " + eventDTO.getEventId() + ", with endTime " + eventDTO.getEndTime());
            return value;
        } catch (Exception e) {
            throw e;
        }
    }

    /* STATUS Table - column names*/

    public void updateStatus(long tripId, boolean isSent){
        ContentValues values = new ContentValues();
        if (isSent){
            values.put(DatabaseOpenHelper.KEY_STATUS,1);
        }else {
            values.put(DatabaseOpenHelper.KEY_STATUS,1);
        }

        try {
            getOpenedDatabase().update(
                    DatabaseOpenHelper.TABLE_STATUS,
                    values,
                    DatabaseOpenHelper.KEY_TRIP_NAME + " = ?",
                    new String[]{String.valueOf(tripId)});
            Log.i(TAG, "A Status has been updated : for TripId " + tripId + ", to"+ String.valueOf(isSent));
        } catch (Exception e) {
            throw e;
        }
    }
}
