package com.mobithink.velo.carbon.database.model;

import java.io.Serializable;


public class RollingPointDTO implements Serializable {

    private Long rollingPointId;
    private Long tripId;
    private Long timeOfRollingPoint;
    private Double gpsLat;
    private Double gpsLong;

    public RollingPointDTO() {
    }
    public Long getRollingPointId() {
        return rollingPointId;
    }
    public void setRollingPointId(Long rollingPointId) {
        this.rollingPointId = rollingPointId;
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }

    public Long getTimeOfRollingPoint() {
        return timeOfRollingPoint;
    }

    public void setTimeOfRollingPoint(Long timeOfRollingPoint) {
        this.timeOfRollingPoint = timeOfRollingPoint;
    }

    public Double getGpsLat() {
        return gpsLat;
    }

    public void setGpsLat(Double gpsLat) {
        this.gpsLat = gpsLat;
    }

    public Double getGpsLong() {
        return gpsLong;
    }

    public void setGpsLong(Double gpsLong) {
        this.gpsLong = gpsLong;
    }
}
