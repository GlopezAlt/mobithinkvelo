package com.mobithink.velo.carbon.driving;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.text.emoji.EmojiCompat;
import android.support.text.emoji.bundled.BundledEmojiCompatConfig;
import android.support.text.emoji.widget.EmojiButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.mobithink.velo.carbon.R;
import com.mobithink.velo.carbon.SplashScreenActivity;
import com.mobithink.velo.carbon.database.model.EventDTO;
import com.mobithink.velo.carbon.database.model.TripDTO;
import com.mobithink.velo.carbon.gopro.GoProController;
import com.mobithink.velo.carbon.gopro.OnTaskCompleted;
import com.mobithink.velo.carbon.managers.CarbonApplicationManager;
import com.mobithink.velo.carbon.managers.DatabaseManager;
import com.mobithink.velo.carbon.managers.RetrofitManager;
import com.mobithink.velo.carbon.services.LocationService;
import com.mobithink.velo.carbon.utils.CustomPopup;
import com.mobithink.velo.carbon.webservices.TripService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DrivingActivity extends AppCompatActivity implements OnTaskCompleted, OnSuccessListener<Location> {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 10;
    private static final int SPEACH_RECOGNITION_TAG =100;

    public static final String CODE_ENVOI_TAG="code_envoi_tag";
    public static final int ENVOI_KO = 0;
    public static final int ENVOI_DECLINE=1;
    public static final int ENVOI_OK=2;

    public static final int CRUD_CREATE=1;
    public static final int CRUD_UPDATE=2;

    private int mTypeCRUD;

    private int serverCallsKO=0;
    private final int SERVER_CALLS_LIMITE=5;

    private long tripId;
    private  EventDTO mCurrentEventDTO;
    private  EventDTO mAmenagementEventDTO;

    private ConnectivityManager connectivityManager;
    private Network wifiNetwork = null;
    private Network dataNetwork=null;

    private ArrayList<String> listeProblemesEvenements;

    private int mAmenagementActiveButtonId =0;
    private View mAmenagementActiveButton;

    private FusedLocationProviderClient mFusedLocationClient;

    private GoProController goProController;

    private View mRootView;
    @BindView(R.id.moins)
    ImageButton moinsIb;
    @BindView(R.id.plus)
    ImageButton plusIb;
    @BindView(R.id.finish_trip)
    ImageButton finishTripIb;
    @BindView(R.id.voie_partage)
    Button voiePartageBt;
    @BindView(R.id.bande)
    Button bandeBt;
    @BindView(R.id.contresens)
    Button contreSensBt;
    @BindView(R.id.couloir_bus)
    Button couloirBusBt;
    @BindView(R.id.piste)
    Button pisteBt;
    @BindView(R.id.voie_verte)
    Button voieVerte;
    @BindView(R.id.vocal)
    ImageButton vocalBt;
    private CustomPopup angryPopup;
    private CustomPopup happyPopup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_driving);
        ButterKnife.bind(this);

        //Manager permettant de gérer la connectivité avec la  GoPRO.
        connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        //Nous creons un request qui va etre transporte par le WIFI
        NetworkRequest.Builder req = new NetworkRequest.Builder();
        req.addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);
        req.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
        connectivityManager.requestNetwork(req.build(), new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                wifiNetwork=network;
            }

        });
        goProController= new GoProController(this,this);

        tripId=CarbonApplicationManager.getInstance().getCurrentTripId();

        EmojiCompat.Config config = new BundledEmojiCompatConfig(this)
                .setEmojiSpanIndicatorEnabled(true);
        EmojiCompat.init(config);

        AlertDialog.Builder builder = new AlertDialog.Builder(DrivingActivity.this);

        angryPopup = new CustomPopup(this,R.layout.angry);
        happyPopup = new CustomPopup(this,R.layout.smile);
        mRootView = findViewById(R.id.rootview);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        setUpButtons();
        setUpListesEvenement();

        Intent serviceIntent = new Intent(this, LocationService.class);
        startService(serviceIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this,LocationService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this,LocationService.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SPEACH_RECOGNITION_TAG:
                traitementControlVocal(resultCode, data);
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION:
                getLastKnownLocationForEvent();
        }
    }

    /**
     * Méthode pour paramétrer les boutons et leur Listeners
     */
    private void setUpButtons(){

        couloirBusBt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mAmenagementActiveButtonId == 0 || v.getId()== mAmenagementActiveButtonId){
                    manageEventAmenagement(v,getString(R.string.couloir_Bus));
                    return true;
                }
                return false;
            }
        });

        voieVerte.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mAmenagementActiveButtonId == 0 || v.getId()== mAmenagementActiveButtonId){
                    manageEventAmenagement(v,getString(R.string.voie_verte));
                    return true;
                }

                return false;
            }
        });

        pisteBt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mAmenagementActiveButtonId == 0 || v.getId()== mAmenagementActiveButtonId){
                    manageEventAmenagement(v, getString(R.string.piste));
                    return true;
                }
                return false;
            }
        });

        contreSensBt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mAmenagementActiveButtonId == 0 || v.getId()== mAmenagementActiveButtonId){
                    manageEventAmenagement(v,getString(R.string.contresens_cyclable));
                    return true;
                }
                return false;
            }
        });

        bandeBt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mAmenagementActiveButtonId == 0 || v.getId()== mAmenagementActiveButtonId){
                    manageEventAmenagement(v,getString(R.string.bande));
                    return true;
                }
                return false;
            }
        });

        voiePartageBt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mAmenagementActiveButtonId == 0 || v.getId()== mAmenagementActiveButtonId){
                    manageEventAmenagement(v,getString(R.string.voie_partagée));
                    return true;
                }
                return false;

            }
        });

        finishTripIb.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                endTrip();
                return true;
            }
        });

        vocalBt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                getSpeechInput();
                return true;
            }
        });


        moinsIb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customPopupBoutonSetUp();
            }
        });

        plusIb.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                playTone(true);
                return true;
            }
        });
    }

    /**
     * Methode pour Creer ou MAJ une evenment du type Amenagemt
     * @param v Bouton cliqué
     * @param eventName Nom de l'evenement
     */
    private void manageEventAmenagement(View v, String eventName) {
        if (mAmenagementActiveButtonId ==0){
            //Création nouvel aménagement
            mCurrentEventDTO = new EventDTO();
            mCurrentEventDTO.setEventName(eventName);
            mCurrentEventDTO.setEventType(getString(R.string.amenagement));
            mTypeCRUD=CRUD_CREATE;

        }else{
            //Fin et MAJ de l’aménagement
            mTypeCRUD=CRUD_UPDATE;
        }
        mAmenagementActiveButtonId=v.getId();
        mAmenagementActiveButton=v;
        getLastKnownLocationForEvent();
    }

    /**
     * Modification du couleur du boutton
     */
    private void changeButtonColor(){
        int color = ((ColorDrawable) mAmenagementActiveButton.getBackground()).getColor();
        if (color==getColor(R.color.colorPrimary)){
            mAmenagementActiveButton.setBackgroundColor(getColor(R.color.colorAccent));
            mAmenagementActiveButtonId =mAmenagementActiveButton.getId();
        }else {
            mAmenagementActiveButton.setBackgroundColor(getColor(R.color.colorPrimary));
            mAmenagementActiveButtonId =0;
        }
    }

    /**
     * Creation du menu emergent pour afficher les emojis
     */
    private void customPopupBoutonSetUp() {
        angryPopup.build();
        EmojiButton paniqueButton = angryPopup.getEmojiButton(R.id.panique);
        StringBuffer stringBuffer = new StringBuffer(new String(Character.toChars(0x1F625	)));
        Log.d("SB", stringBuffer.toString());
        paniqueButton.setText(stringBuffer);
        paniqueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emojiButtonClick(v.getId());
            }
        });

        EmojiButton anxieuxButton = angryPopup.getEmojiButton(R.id.anxieux);
        stringBuffer = new StringBuffer(new String(Character.toChars(0x1F613	)));
        Log.d("SB", stringBuffer.toString());
        anxieuxButton.setText(stringBuffer);
        anxieuxButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emojiButtonClick(v.getId());
            }
        });

        EmojiButton agaceButton = angryPopup.getEmojiButton(R.id.agace);
        stringBuffer = new StringBuffer(new String(Character.toChars(0x1F623	)));
        Log.d("SB", stringBuffer.toString());
        agaceButton.setText(stringBuffer);
        agaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emojiButtonClick(v.getId());
            }
        });


        EmojiButton deconcerteButton = angryPopup.getEmojiButton(R.id.deconcerte);
        stringBuffer = new StringBuffer(new String(Character.toChars(0x1F616	)));
        Log.d("SB", stringBuffer.toString());
        deconcerteButton.setText(stringBuffer);
        deconcerteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emojiButtonClick(v.getId());
            }
        });
    }

    private void emojiButtonClick(int id) {
        String emoji= null;
        switch (id){
            case R.id.panique:
                emoji=getString(R.string.panique).toLowerCase();
                break;
            case R.id.anxieux:
                emoji=getString(R.string.anxieux).toLowerCase();
                break;

            case R.id.agace:
                emoji=getString(R.string.agace).toLowerCase();
                break;

            case R.id.deconcerte:
                emoji=getString(R.string.deconcentre).toLowerCase();
                break;

             default:
                 Log.e("error", "unsuported emoji");
                 break;
        }

        if (emoji!=null){
            mCurrentEventDTO= new EventDTO();
            mCurrentEventDTO.setEventName(emoji);
            mCurrentEventDTO.setEventType(getString(R.string.ressenti));
            mTypeCRUD=CRUD_CREATE;
            getLastKnownLocationForEvent();
            angryPopup.dismiss();


        }

    }

    /**
     * Methode pour recuperer les listes de commands vocales
     */
    private void setUpListesEvenement() {
        listeProblemesEvenements= new ArrayList<>();
        String[] array;
        //Récupération de la liste d’évènements prédéfinis du type probeleme
        array = getResources().getStringArray(R.array.problemes_array);
        listeProblemesEvenements.addAll(Arrays.asList(array));
    }

    /**
     *  Ajout d’un évènement dans la base de donnes local
     * @param eventDTO eventment a creer
     */
    private void createEvent(EventDTO eventDTO) {

        eventDTO.setStartTime(System.currentTimeMillis());
        eventDTO.setTripId(tripId);
        long newId=DatabaseManager.getInstance().createNewEvent(eventDTO);
        if (newId>0){
            eventDTO.setEventId(newId);
            //si l’évènement est du type aménagement on notifie le départ et on MAJ les buttons, sinon on affiche le bon enregistrement
            if (eventDTO.getEventType().equals(getString(R.string.amenagement))){
                Snackbar.make(mRootView,getString(R.string.depart_amenagement)+ eventDTO.getEventName(), Snackbar.LENGTH_LONG).show();
                mAmenagementEventDTO=eventDTO;
                changeButtonColor();
            }else {
                Snackbar.make(mRootView,getString(R.string.evenement_enregistre)+ eventDTO.getEventName(), Snackbar.LENGTH_LONG).show();
            }
            //On prendre un photo suelement si c'est du type probleme
            if (eventDTO.getEventType().equals(getString(R.string.probleme))){
                takePhoto();
            }
            playTone(true);
        }else {
            playTone(false);
        }
    }

    /**
     * Methode pour MAJ les evenement du type amenagment
     */
    private void updateEvent(EventDTO eventDTO) {
        eventDTO.setEndTime(System.currentTimeMillis());
        int sqlReturn=DatabaseManager.getInstance().updateEvent(eventDTO);
        if (sqlReturn>0){
            Snackbar.make(mRootView,getString(R.string.evenement_enregistre), Snackbar.LENGTH_SHORT).show();
            changeButtonColor();
            playTone(true);
        }else {
            playTone(false);
        }
    }

    /**
     * methode pour envoyer la requette HTTP pour prendre un photo
     */
    private void takePhoto() {
        if (wifiNetwork!=null) {
            connectivityManager.bindProcessToNetwork(wifiNetwork);
            goProController.startTakePhoto();
        }
    }

    public void endTrip() {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setTripID(tripId);
        tripDTO.setEndTime(System.currentTimeMillis());
        DatabaseManager.getInstance().updateTrip(tripDTO);
        stopService(new Intent(this, LocationService.class));
        showSendDataDialog();
    }

    private void showSendDataDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setTitle("Saisie terminée");
        alertDialogBuilder.setMessage("Fin de saisie pour la ligne, souhaitez vous envoyer les données au serveur ?");
        alertDialogBuilder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // permets reveiller le serveur, si Ok on envoi le TRIP
                sendTripDto();
                dialog.cancel();
            }
        });

        alertDialogBuilder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatabaseManager.getInstance().updateStatus(tripId,false);
                returnToSplashScreenActivity(ENVOI_DECLINE);
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     *
     */
    private void sendTripDto() {

        TripDTO tripDto = DatabaseManager.getInstance().getFullTripDTODataToSend(tripId);

        TripService tripService = RetrofitManager.build().create(TripService.class);
        Call<Void> call = tripService.register(tripDto);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                switch (response.code()) {
                    case 201:
                        setResult(RESULT_OK);
                        DatabaseManager.getInstance().updateStatus(tripId,true);
                        returnToSplashScreenActivity(ENVOI_OK);
                        break;
                    default:
                        Log.e(getString(R.string.send_trip),getString(R.string.response_serveur)+response.code());
                        DatabaseManager.getInstance().updateStatus(tripId,false);
                        returnToSplashScreenActivity(ENVOI_KO);
                        break;
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Snackbar.make(mRootView, getString(R.string.evoi_echoue), Snackbar.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Recuperation du commande vocale
     */
    private void getSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, SPEACH_RECOGNITION_TAG);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Ce Méthode permet de gérer les commandes vocales.
     * Si le command retourné par le service est parmi la liste des évènements prédéfinis le traitement va créer un évènement spécifique
     * Sinon, un voice memo sera créé en utilisant un évènement du type générique
     * @param resultCode Retour de l'API
     * @param data L'intent avec le resultat de la conversion vocal vers text
     */
    private void traitementControlVocal(int resultCode, Intent data){
        if (resultCode == RESULT_OK && data != null) {
            mCurrentEventDTO = new EventDTO();
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String commandeVocale=result.get(0);
            mTypeCRUD=CRUD_CREATE;
            if (listeProblemesEvenements.contains(commandeVocale.toLowerCase())){
                //si c'est une action prédéfini du type probeleme
                mCurrentEventDTO.setEventType( getString(R.string.probleme));
                mCurrentEventDTO.setEventName(commandeVocale);

            }else {
                //Sinon, on cree un evenenemnt generique pour ajouter une voice memo
                mCurrentEventDTO.setEventName(getString(R.string.action_generique));
                mCurrentEventDTO.setVoiceMemo(commandeVocale);
                mCurrentEventDTO.setEventType(getString(R.string.voice_memo));
            }
            getLastKnownLocationForEvent();
        }
        else {
            Snackbar.make(mRootView,getString(R.string.erreur_commande_vocale), Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     *
     * @param isOK
     */
    private void playTone(boolean isOK){
        MediaPlayer mediaPlayer;
        if (isOK){
             mediaPlayer= MediaPlayer.create(this, R.raw.tone_ok);
        }else {
             mediaPlayer = MediaPlayer.create(this, R.raw.tone_ko);
        }
        mediaPlayer.start();

    }
    /**
     * Recuperation de l'ubication
     */
    private void getLastKnownLocationForEvent(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            askLocationServicesPermission();
        }
        Task<Location> lastLocation = mFusedLocationClient.getLastLocation();
        lastLocation.addOnSuccessListener(this);
    }

    /**
     * Methode pour demander le droit d'acces a l'ubucation
     */
    private void askLocationServicesPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission_group.LOCATION)) {

            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.permission_is_needed))
                    .setMessage(getString(R.string.why_permission_is_needed))
                    .setPositiveButton(getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(DrivingActivity.this,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        }
                    })
                    .setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create()
                    .show();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    /**
     * Méthode pour revenir à l’activité principale.
     * Code envoi correspond aux valeurs suivants
     * Erreur Transmition
     * Envoi Decline par l’utilisatuer
     * Enovi Correcte
     * @param codeEnvoi int pour notifier quel message afficher a l'utilisateur dans l'activite princiaple
     */
    private void returnToSplashScreenActivity(int codeEnvoi){
        Bundle bundle = new Bundle();
        bundle.putInt(CODE_ENVOI_TAG,codeEnvoi);
        Intent intent = new Intent(this, SplashScreenActivity.class);
        intent.putExtras(bundle);
        this.startActivity(intent);

    }

    /**
     * Listener du retour de la requette HTTP de la GoPro
     */
    @Override
    public void onTaskCompleted(boolean wasPhotoTaken) {
        if(wasPhotoTaken){
            if (dataNetwork!=null){
                connectivityManager.bindProcessToNetwork(dataNetwork);
            }
        }
    }

    /**
     * Listener pour l'ubication
     * @param location
     */
    @Override
    public void onSuccess(Location location) {
        if (location== null){
            Log.e("GPS","Location is null");
            getLastKnownLocationForEvent();
            return;
        }
        if (mTypeCRUD==CRUD_CREATE){
            mCurrentEventDTO.setGpsLatStart(location.getLatitude());
            mCurrentEventDTO.setGpsLongStart(location.getLongitude());
            createEvent(mCurrentEventDTO);
        }else {
            mAmenagementEventDTO.setGpsLatEnd(location.getLatitude());
            mAmenagementEventDTO.setGpsLongEnd(location.getLongitude());
            updateEvent(mAmenagementEventDTO);
        }
    }
}
